# **html元素居中的方法**

我们在开发前端页面的时候经常会碰到要把某个元素居中，但有时候却是想归想，做起来却无从下手。那这肿么办呢？

仔细想想，办法其实还是有很多的！比方说下面的这几个方法......

### 负外边距居中

什么是负外边距剧中呢？其实所谓的负外边距方法也就是把元素的margin值设为负值。具体实现方法大家可以往下看

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>负外边距居中方法</title>
	<style>
		html,body{
			margin: 0;
			padding: 0;
		}
		.box{
			width: 500px;
			height: 350px;
			position: absolute;
			top: 50%;
			left: 50%;
			margin-left: -250px;
			margin-top: -175px;
			color: #fff;
			background: #888;
			border: 1px solid #c00;
		}
	</style>
</head>
<body>
	<div class="box">
		<div class="small">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat qui recusandae, numquam! Non quas at nemo adipisci magni cumque saepe, atque, beatae impedit, esse doloremque aliquid nulla exercitationem sed sequi.
		</div>
	</div>
</body>
</html>
```

##### 值得注意的是：在使用此方法的时候必须保证该元素具有固定的width和height。而且我们也可以看到元素的margin值被设置为了元素宽高的一半并且还是负值,此外元素还得使用position定位。



### 绝对定位方法居中

顾名思义，此方法就是使用position来实现元素居中，接着往下看

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>绝对定位方法</title>
	<style>
		html,body{
			margin:0;
			padding: 0;
		}
		.box{
			width: 500px;
			height: 350px;
			background: #c00;
			border: 1px solid #888;
			position: absolute;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			margin: auto;
		}
	</style>
</head>
<body>
	<div class="box">
		<div class="small">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores ipsum facilis voluptatibus non rerum explicabo tempora blanditiis optio, suscipit at, placeat harum itaque maiores vitae nobis commodi illo, esse accusantium.
		</div>
	</div>
</body>
</html>
```

看到这里大家就都明白了，这个方法就是充分使用了position属性来使元素居中。

##### 当然也少不了要给元素width和height；并且还是的给元素设置margin不过此时的值为auto;还有一个最大的不同在于此方法的position的四个属性全都设置了。

### 接下来，就是使用css的transform方法来居中

这个方法就是使用css 的transform属性实现的，具体怎样实现？

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>transform居中方法</title>
	<style>
		html,body{
			margin: 0;
			padding: 0;
		}
		.box{
			width: 500px;
			/*height: 350px;*/
			background: pink;
			color: #888;
			border: 1px solid #c00;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%,-50%);
		}
	</style>
</head>
<body>
	<div class="box">
		<div class="small">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae alias saepe, quasi deleniti, delectus impedit? Expedita distinctio perferendis magni a, impedit iusto possimus sapiente, quia, nisi, officiis nostrum dolor eum.
		</div>
	</div>
</body>
</html>
```

##### 这个方法就是使用transform的translate()实现。但也是元素必须的有position定位才可以。



### 最后给大家介绍一个比较流行的方法，那就是使用flex布局实现

##### 如果大家还不了解flex布局可以参考阮一峰老师的这篇博客：

[http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html?^%$]: 

具体实现：

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>flex实现居中</title>
	<style>
		.box{
			background: pink;
			width: 500px;
			height: 350px;
			border: 1px solid #c00;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		.small{
			color: #fff;
			border: 1px solid black;
		}
	</style>
</head>
<body>
	<div class="box">
		<div class="small">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit suscipit ducimus aut quibusdam quidem odio quae praesentium magni, natus sed placeat earum, eius ipsum, rem, fuga quia labore qui fugiat.
		</div>
	</div>
</body>
</html>
```

##### 此方法充分使用了flex的布局理念，这个方法也是目前比较流行的一个方法，再次推荐大家使用这个方法。

